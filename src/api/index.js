const productData = require('./products')

export function getAllProducts () {
  return productData
}

export function getProductsInSize (size) {
  let products = []

  // if all product data requested, return all
  if (size === 'all') {
    products = productData

  // if specific size requested, filter through product JSON
  } else {
    for (const product of productData) {
      if (product.size.indexOf(size) > -1) {
        products.push(product)
      }
    }
  }

  return products
}

export function getAllSizes () {
  let sizes = []
  // scan through JSON file for all the different sizes in the products
  for (const product of productData) {
    for (const size of product.size) {
      if (sizes.indexOf(size) === -1) { // ensure there aren't duplicates
        sizes.push(size)
      }
    }
  }
  return sizes
}
