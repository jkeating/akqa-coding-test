# AKQA Coding Test

This is the repository contains the AKQA coding test, completed by myself, Joshua Keating.

See [here for a staging environment](http://akqa.joshs.space/) running the build files.

## Build Setup

``` bash
# install dependencies
npm install
# or
yarn install

# serve with hot reload at localhost:8080
npm run dev
# or
yarn dev

# build for production with minification
npm run build
# or
yarn build
```
## Project Notes

[Vue.js](https://vuejs.org/) is the MV* framework used in this project, and was chosen simply because I am most familiarity with it. I would have chosen React JS, had fear of scrutiny and my common sense not kicked in.

The project was instantiated using the `vue-cli` command line tool, where I used the [webpack-simple](https://github.com/vuejs-templates/webpack-simple) starter setup to kick things off.

I've tried to keep things intuitive and laid out the project in a way which separates concerns. Vue components are in the `src/components` folder, assets are stored in `src/assets`, and API / data related logic is contained within the `src/api` folder.

The structure of my Vue components are generally self-contained, and follow a similar format to what is outlined below:

```
<style>
 ...
</style>

<template>
 ...
</template>

<script>
 ...
</script>
```

## In Hindsight

It would have been *slightly* cleaner to extrapolate my `scss` out into separate files, which would also allow for re-use of `scss` variables, which I haven't done in this project... Although there is no doubt a way.

I would have been nice to throw in some unit tests to ensure my logic within the `src/api` folder was correct, but hey, dinner calls...
